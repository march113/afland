﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpCameraView : MonoBehaviour {
    public Camera maincamera;
    WebCamTexture webcamTexture;
    const int FPS = 60;

    // Use this for initialization
    void Start() {
        //Quadを画面いっぱいに広げる
        transform.position = maincamera.transform.position;
        transform.position += maincamera.transform.forward;
        float _h = maincamera.orthographicSize * 2;
        float _w = _h * maincamera.aspect;
        string[] screenres = UnityStats.screenRes.Split('x');
        //スマホ(Unity)が横ならそのまま
        Debug.Log(_h);
        Debug.Log(_w);
        //if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft) {
            transform.localScale = new Vector3(_w, _h, 1);
        //}
        //縦なら回転させる
        //if (Input.deviceOrientation == DeviceOrientation.FaceUp) {
        //    transform.localScale = new Vector3(_h, _w, 1);
        //    transform.localRotation *= Quaternion.Euler(0, 0, -90);
        //}
        //カメラのテクスチャをQuadに載せる
        Renderer rend = GetComponent<Renderer>();
        Debug.Log(WebCamTexture.devices.Length);
        if (WebCamTexture.devices.Length > 0) {
            WebCamDevice cam = WebCamTexture.devices[1];
            WebCamTexture wcam = new WebCamTexture(cam.name);
            wcam.Play();
            int width = wcam.width, height = wcam.height;
            webcamTexture = new WebCamTexture(cam.name, width, height, FPS);
            wcam.Stop();

            rend.material.mainTexture = webcamTexture;
            webcamTexture.Play();
        }

    }

    // Update is called once per frame
    void Update() {

    }
}
