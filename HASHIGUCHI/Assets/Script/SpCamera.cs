﻿using UnityEngine;
using System.Collections;

public class SpCamera : MonoBehaviour {
    private Vector3 lastMousePosition;
    // Use this for initialization
    void Start() {
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update() {
        GyroCamera();

    }
    void GyroCamera() {
        if (Input.gyro.enabled) {
            Quaternion gyro = Input.gyro.attitude;
            this.transform.localRotation = Quaternion.Euler(90, 0, 0) * (new Quaternion(-gyro.x * Time.deltaTime, -gyro.y * Time.deltaTime, gyro.z * Time.deltaTime, gyro.w * Time.deltaTime));
        }
    }
}